#!/usr/bin/python
import yoda
import math
import os
import glob
from optparse import OptionParser
import numpy as np

def evaluate_C1 (infile,proc,outputdir,pdfset=None, merge=False, altpdfset=None):

    if not (os.path.isdir(outputdir)): os.system("mkdir %s"%(outputdir))

    lhapdfdir = os.environ['LHAPDF_DATA_PATH']
    if lhapdfdir == '':
        print "warning: env variable LHAPDF_DATA_PATH is not set"
        print "> assuming it to be ./MG5_aMC_v2_5_5/HEPTools/lhapdf6/share/LHAPDF/"
        lhapdfdir = './MG5_aMC_v2_5_5/HEPTools/lhapdf6/share/LHAPDF/'


    stage1_2_bin_list =[]
    stage1_2_fine_bin_list=[]
    stage1_2_fine=False
    stage1_2=False
    f = open(os.environ.get('RIVET_ANALYSIS_PATH')+"/HiggsTemplateCrossSections.h", "r") 
    for line in f:
        if "namespace Stage1_2 {" in line: stage1_2= True
        if "} // namespace Stage1_2" in line: stage1_2= False
        if "namespace Stage1_2_Fine {" in line: stage1_2_fine= True
        if "} // namespace Stage1_2_Fine" in line: stage1_2_fine= False
        
        if  stage1_2_fine:
            if "=" in line: stage1_2_fine_bin_list.append(line.split("=")[0].replace(" ",""))    
        if  stage1_2:
            if "=" in line: stage1_2_bin_list.append(line.split("=")[0].replace(" ",""))

    scale_variations=[
        "MUR=0.50000E+00_MUF=0.50000E+00_MERGING=0.000",
        "MUR=0.10000E+01_MUF=0.50000E+00_MERGING=0.000",
        "MUR=0.50000E+00_MUF=0.10000E+01_MERGING=0.000",
        "MUR=0.20000E+01_MUF=0.10000E+01_MERGING=0.000",
        "MUR=0.10000E+01_MUF=0.20000E+01_MERGING=0.000",
        "MUR=0.20000E+01_MUF=0.20000E+01_MERGING=0.000",
    ]
    alt_pdf_variations=[]
    if altpdfset:
        for pdf in altpdfset.split(','):
            with open( lhapdfdir+"/pdfsets.index") as f:
                for line in f:
                    if pdf in line:
                        alt_pdf_variations.append(str(line.split()[0])+"_"+line.split()[1].upper()+"_MERGING=0.000")
    else:
        print ("WARNING: alterantive alphaS PDFset variation not considered. If included in the generation please set nominal pdf used for variations --altpdfset option")
    pdf_variations=[]
    if pdfset:
        num_variations=0
        pdf_name=''
        pdf_rivet_name=''
        with open( lhapdfdir+"/pdfsets.index") as f:
            for line in f:
                if pdfset in line:
                    pdf_name=str(line.split()[1])
                    pdf_rivet_name=line.split()[1].upper()+"_MERGING=0.000"
        if pdf_name=='':
            raise Exception("pdfset %s not found in %s"%(pdfset,lhapdfdir+"/pdfsets.index"))
        with open( lhapdfdir+"/"+pdf_name+"/"+pdf_name+".info") as f:
            for line in f:
                if "NumMembers:" in line:
                    num_variations=int(line.split()[1])
        for i in range(num_variations):
            pdf_variations.append(str(int(pdfset)+i)+"_"+ pdf_rivet_name)
    else:
        print ("WARNING: alterantive PDFset variation not considered. If included in the generation please set nominal pdf used for variations with --pdfset option")
   

    # yodaAOs = yoda.read("/storage_tmp/atlas/smanzoni/SelfCoupling/self-coupling-c1_repo_ttH/ZH_condor_v1/Rivet_400-600.yoda")
    #yoda_LO_AOs = yoda.read("/storage_tmp/atlas/smanzoni/SelfCoupling/self-coupling-c1_repo_ttH/ZH_condor_v1/Rivet_0-200.yoda")
    yodaAOs = yoda.read(infile)
    fixed_scale="" 
    if '/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[0_MUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000]' in yodaAOs:
        fixed_scale="0_"        
    yodaAO_LO = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%sMUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000]'%fixed_scale]; 
    yodaAO_NLO = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[KL_MUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000]'];  
    total_LO=0.
    total_NLO=0.
    total_LO_error=0.
    total_NLO_error=0.
    filename=outputdir+'/'+proc+"_stage1_2_pTjet30.txt"
    if merge:  filename=outputdir+'/'+proc+"_merge_stage1_2_pTjet30.txt"
    with open(filename, 'w') as f:
        f.write("STXS BIN C1[%] stat scale pdf\n")    
        for i in range(0,len(stage1_2_bin_list)):
            total_LO+=yodaAO_LO.bin(i).sumW()
            total_NLO+=yodaAO_NLO.bin(i).sumW()
            total_LO_error+=yodaAO_LO.bin(i).sumW2()
            total_NLO_error+=yodaAO_NLO.bin(i).sumW2()
            if ("1J" in stage1_2_bin_list[i]) and merge and ("ZH" in options.proc or "WH" in options.proc): continue
            if yodaAO_LO.bin(i).sumW()!=0:
                LO=yodaAO_LO.bin(i).sumW()
                NLO=yodaAO_NLO.bin(i).sumW()
                sigmaNLO=yodaAO_NLO.bin(i).sumW2()
                sigmaLO=yodaAO_LO.bin(i).sumW2()
                if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_bin_list[i]:
                    LO+=yodaAO_LO.bin(i+1).sumW()
                    NLO+=yodaAO_NLO.bin(i+1).sumW()
                    sigmaNLO+=yodaAO_NLO.bin(i+1).sumW2()
                    sigmaLO+=yodaAO_LO.bin(i+1).sumW2()
                    
                sigmaNLO_div_NLO=(math.sqrt(sigmaNLO)/NLO)
                sigmaLO_div_LO=(math.sqrt(sigmaLO)/LO)
                C1=NLO/LO*100.
                error = C1*math.sqrt((sigmaNLO_div_NLO**2)+(sigmaLO_div_LO**2))#-2*((math.sqrt(sigmaNLO)*math.sqrt(sigmaLO))/(LO*NLO)) )
                max_error_scale=0.
                error_pdf=0
                max_error_alt=0
                for scale in scale_variations:
                    yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s%s]'%(fixed_scale,scale)];  
                    yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[KL_%s]'%scale];
                    C1_v=yodaAO_NLO_v.bin(i).sumW()/yodaAO_LO_v.bin(i).sumW()*100
                    if abs(C1-C1_v)>max_error_scale:  max_error_scale=abs(C1-C1_v)

                error_pdf_list=[]
                for pdf in pdf_variations:
                    yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%(pdf)];  
                    yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%pdf.replace("_MERGING","_KL_MERGING")];
                    LO_v=yodaAO_LO_v.bin(i).sumW()
                    NLO_v=yodaAO_NLO_v.bin(i).sumW()
                    if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_bin_list[i]:
                        LO_v+=yodaAO_LO_v.bin(i+1).sumW()
                        NLO_v+=yodaAO_NLO_v.bin(i+1).sumW()
                    error_pdf_list.append(NLO_v/LO_v*100.)
                error_pdf=np.std(error_pdf_list)

                for pdf in alt_pdf_variations:
                    try: yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%(pdf)];
                    except:  yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%("_".join(pdf.split("_")[1:]))]; 
                    try: yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%pdf.replace("_MERGING","_KL_MERGING")];
                    except: yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_pTjet30[%s]'%("_".join(pdf.split("_")[1:])).replace("_MERGING","_KL_MERGING")];
                    LO_v=yodaAO_LO_v.bin(i).sumW()
                    NLO_v=yodaAO_NLO_v.bin(i).sumW()
                    if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_bin_list[i]:
                        LO_v+=yodaAO_LO_v.bin(i+1).sumW()
                        NLO_v+=yodaAO_NLO_v.bin(i+1).sumW()
                    C1_v=NLO_v/LO_v*100.
                    if abs(C1-C1_v)>max_error_alt:  max_error_alt=abs(C1-C1_v)

                max_error_pdf=math.sqrt(error_pdf**2+max_error_alt**2)
                if ("ZH" in options.proc or "WH" in options.proc) and merge:
                    f.write(stage1_2_bin_list[i].replace("_0J","")+" = "+"{0:.5f}".format(C1)+" +/- "+"{0:.5f}".format(error)+" +/- "+"{0:.5f}".format(max_error_scale)+" +/- "+"{0:.5f}".format(max_error_pdf)+"\n")
                else: f.write(stage1_2_bin_list[i]+" = "+"{0:.5f}".format(C1)+" +/- "+"{0:.5f}".format(error)+" +/- "+"{0:.5f}".format(max_error_scale)+" +/- "+"{0:.5f}".format(max_error_pdf)+"\n")
                #else: f.write(stage1_2_bin_list[i]+" = 0 \n")
        sigmaNLO_div_NLO=(math.sqrt(total_NLO_error)/total_NLO)
        sigmaLO_div_LO=(math.sqrt(total_LO_error)/total_LO)
        total_error = math.sqrt((sigmaNLO_div_NLO**2+sigmaLO_div_LO**2))*(total_NLO/total_LO)
        f.write("total: {0:.5f}".format(total_NLO/total_LO*100.)+" +/- "+"{0:.5f}".format(total_error*100.))

    yodaAO_LO = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%sMUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000]'%fixed_scale]
    yodaAO_NLO = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[KL_MUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000]']
    filename=outputdir+'/'+proc+"_stage1_2_fine_pTjet30.txt"
    if merge:  filename=outputdir+'/'+proc+"_merge_stage1_2_fine_pTjet30.txt"
    with open(filename, 'w') as f:
        f.write("STXS BIN C1[%] stat scale pdf\n")    
        for i in range(0,len(stage1_2_fine_bin_list)):
            if ("1J" in stage1_2_fine_bin_list[i] or "2J" in stage1_2_fine_bin_list[i]) and merge and ("ZH" in options.proc or "WH" in options.proc): continue
            if yodaAO_LO.bin(i).sumW()!=0: 
                LO=yodaAO_LO.bin(i).sumW()
                NLO=yodaAO_NLO.bin(i).sumW()
                sigmaNLO=yodaAO_NLO.bin(i).sumW2()
                sigmaLO=yodaAO_LO.bin(i).sumW2()
                if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_fine_bin_list[i]:
                    for j in [1,2]:
                        LO+=yodaAO_LO.bin(i+5*(j)).sumW()
                        NLO+=yodaAO_NLO.bin(i+5*(j)).sumW()
                        sigmaNLO+=yodaAO_NLO.bin(i+5*(j)).sumW2()
                        sigmaLO+=yodaAO_LO.bin(i+5*(j)).sumW2()
                    
                sigmaNLO_div_NLO=(math.sqrt(sigmaNLO)/NLO)
                sigmaLO_div_LO=(math.sqrt(sigmaLO)/LO)
                C1=NLO/LO*100.
                error = C1*math.sqrt((sigmaNLO_div_NLO**2)+(sigmaLO_div_LO**2))#-2*((math.sqrt(sigmaNLO)*math.sqrt(sigmaLO))/(LO*NLO)) )
                max_error_scale=0.
                error_pdf=0.
                max_error_alt=0.
                for scale in scale_variations:
                    yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s%s]'%(fixed_scale,scale)];  
                    yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[KL_%s]'%scale];
                    LO_v=yodaAO_LO_v.bin(i).sumW()
                    NLO_v=yodaAO_NLO_v.bin(i).sumW()
                    if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_fine_bin_list[i]:
                        for j in [1,2]:
                            LO_v+=yodaAO_LO_v.bin(i+5*(j)).sumW()
                            NLO_v+=yodaAO_NLO_v.bin(i+5*(j)).sumW()
                    C1_v=NLO_v/LO_v*100.
                    if abs(C1-C1_v)>max_error_scale:  max_error_scale=abs(C1-C1_v)

                error_pdf_list=[]
                for pdf in pdf_variations:
                    yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%(pdf)] 
                    yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%pdf.replace("_MERGING","_KL_MERGING")]
                    LO_v=yodaAO_LO_v.bin(i).sumW()
                    NLO_v=yodaAO_NLO_v.bin(i).sumW()
                    if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_fine_bin_list[i]:
                        for j in [1,2]:
                            LO_v+=yodaAO_LO_v.bin(i+5*(j)).sumW()
                            NLO_v+=yodaAO_NLO_v.bin(i+5*(j)).sumW()
                    error_pdf_list.append(NLO_v/LO_v*100.)
                error_pdf=np.std(error_pdf_list)
                    
                for pdf in alt_pdf_variations:
                    try: yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%(pdf)];
                    except:  yodaAO_LO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%("_".join(pdf.split("_")[1:]))]; 
                    try: yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%pdf.replace("_MERGING","_KL_MERGING")];
                    except: yodaAO_NLO_v = yodaAOs['/RAW/HiggsTemplateCrossSections/STXS_stage1_2_fine_pTjet30[%s]'%("_".join(pdf.split("_")[1:])).replace("_MERGING","_KL_MERGING")];
                    LO_v=yodaAO_LO_v.bin(i).sumW()
                    NLO_v=yodaAO_NLO_v.bin(i).sumW()
                    if ("ZH" in options.proc or "WH" in options.proc) and merge and "0J" in stage1_2_fine_bin_list[i]:
                        for j in [1,2]:
                            LO_v+=yodaAO_LO_v.bin(i+5*(j)).sumW()
                            NLO_v+=yodaAO_NLO_v.bin(i+5*(j)).sumW()
                    C1_v=NLO_v/LO_v*100.
                    if abs(C1-C1_v)>max_error_alt:  max_error_alt=abs(C1-C1_v)
                max_error_pdf=math.sqrt(error_pdf**2+max_error_alt**2)
                if ("ZH" in options.proc or "WH" in options.proc) and merge:
                    f.write(stage1_2_fine_bin_list[i].replace("_0J","")+" = "+"{0:.5f}".format(C1)+" +/- "+"{0:.5f}".format(error)+" +/- "+"{0:.5f}".format(max_error_scale)+" +/- "+"{0:.5f}".format(max_error_pdf)+"\n")
                else: f.write(stage1_2_fine_bin_list[i]+" = "+"{0:.5f}".format(C1)+" +/- "+"{0:.5f}".format(error)+" +/- "+"{0:.5f}".format(max_error_scale)+" +/- "+"{0:.5f}".format(max_error_pdf)+"\n")
            #else: f.write(stage1_2_fine_bin_list[i]+" = 0 \n")
        f.write("total: {0:.5f}".format(total_NLO/total_LO*100.)+" +/- "+"{0:.5f}".format(total_error*100.))
    print ("C1 total: {0:.5f}".format(total_NLO/total_LO*100.)+" +/- "+"{0:.5f}".format(total_error*100.))


    




if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--proc", help="Process (Hjj, ZH, WH, ttH, or tHq)")
    parser.add_option("--infile",  help="path to yoda file")
    parser.add_option("--outputdir", help="output directory")
    parser.add_option("--pdfset", default=None, help="nominal and variated pdf used e.g.90500,13100,25200")
    parser.add_option("--altpdfset", default=None, help="alternative pdf for alphas variation")
    parser.add_option("--merge",  default=False,  action="store_true")

    (options,args)=parser.parse_args()
    
    evaluate_C1 (infile=options.infile,proc=options.proc,outputdir=options.outputdir, pdfset=options.pdfset, merge=options.merge, altpdfset=options.altpdfset)
