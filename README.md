# Self Coupling C1
Compute the C1 coefficients for the parametrization of the trilinear Higgs self-coupling effects on the single-Higgs production mechanisms

## Installation

Run the install.sh script to download and install Madgraph+lhapdf+trilinear-RW
```
source install.sh
```    

After installation, and whenever starting a new session setup the environment:
```
source setup.sh
```
  
## Run an example event generation
To make sure that everithing is compiled correctly, and also to produce lib and data that will be used to run the reweight machinery, run the following example:
```
cd MG5_aMC_v2_5_5
./bin/mg5_aMC
    generate p p > t t~
    output test_ttbar
    exit
sed -i 's/nn23lo1    = pdlabel/lhapdf = pdlabel/g' test_ttbar/Cards/run_card.dat
sed -i 's/230000    = lhaid/90500    = lhaid/g' test_ttbar/Cards/run_card.dat
./test_ttbar/bin/generate_events
    shower=OFF
    detector=OFF
    analysis=OFF
    madspin=OFF
    reweight=OFF
cd ..
```
NOTE: do not delete the content of the `test_ttbar` folder because some lib and data will be re-used to run the trilinear reweight machinery.

## Run the event generation + trilinear reweight + showering&hadronization + rivet
A python script can be used to control the full process
```
python submit_trilinear_study.py --proc ZH --store_dir myfirstZHproduction --nevents 10000
```
Run `python submit_trilinear_study.py --help` to have the full list of available settings.

The code can run in parallel on condor batch queue using the option --driver condor.

```
python submit_trilinear_study.py --proc ZH --store_dir myfirstZHproduction_condor --nevents 100000 --njobs 10 --driver condor
```

**NOTE: Do not submit simoultaneosly ttH process in parallel to Hjj,WH and ZH process!**

The final output of the code is the Rive.yoda file which contains the STXS classification histograms for LO+scale-variations and NLO EW+scale-variations. The LO and NLO nominal histograms are identified by 0_MUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000 and KL_MUR=0.10000E+01_MUF=0.10000E+01_MERGING=0.000, respectively

## Evaluate C1

To evaluate C1 terms in STXS bins use the script `evaluate_C1.py`
```
python evaluate_C1.py --infile myfirstZHproduction/Rivet.yoda --proc ZH --outputdir myfirstZHproduction
```
When running in parallel, before using `evaluate_C1.py` merge the several yoda with `yodamerge` command
```
yodamerge -o myfirstZHproduction_condor/Rivet.yoda myfirstZHproduction_condor/condor_*/*.yoda
```
