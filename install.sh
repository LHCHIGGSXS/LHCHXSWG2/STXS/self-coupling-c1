#!/bin/bash

workdir=$PWD

# setup a specific cc7 configuration
source /cvmfs/sft.cern.ch/lcg/views/LCG_93/x86_64-centos7-gcc62-opt/setup.sh

# pythia8 requires specific lhapdf and hepmc versions
# I will parse them specifically
export PYTHIA8=$(/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/245p3-82c36/x86_64-centos7-gcc62-opt/bin/pythia8-config --prefix)
echo "> PYTHIA8 path is "$PYTHIA8
# export PYTHIA8DATA=`$PYTHIA8/bin/pythia8-config --xmldoc`

#get LHAPDF PATH
read -ra ary <<<$($PYTHIA8/bin/pythia8-config --lhapdf6)
LHAPDF_PATH=''
for key in "${ary[@]}"; 
do 
    if [[ $key == '-I'* ]]; then 
	LHAPDF_PATH=`echo $key | sed 's/-I//g' | sed 's/include//g'`
    fi
done
echo "> LHAPDF required by Pythia is "$LHAPDF_PATH

# #get HEPMC PATH
# read -ra ary <<<$($PYTHIA8/bin/pythia8-config --hepmc2)
# HEPMC_PATH=''
# for key in "${ary[@]}"; 
# do 
#     if [[ $key == '-I'* ]]; then 
# 	HEPMC_PATH=`echo $key | sed 's/-I//g' | sed 's/include//g'`
#     fi
# done
# echo "> HEPMC required by Pythia is "$HEPMC_PATH


# setup LHAPDF
export PATH=:$LHAPDF_PATH/bin/:$PATH
export LD_LIBRARY_PATH=$LHAPDF_PATH/lib/:$LD_LIBRARY_PATH
export PYTHONPATH=`ls $LHAPDF_PATH/lib/python*/site-packages/ -d`:$PYTHONPATH

# The lhapdf sets and the corresponding config file are in different folders 
# This is problematic for our mg5 version https://sft.its.cern.ch/jira/si/jira.issueviews:issue-html/SPI-959/SPI-959.html
# We will create locally a folder and link the config files and the pdf sets of interest
mkdir $workdir/LHAPDFsets
ln -s $LHAPDF_PATH/share/LHAPDF/pdfsets.index $workdir/LHAPDFsets/pdfsets.index
ln -s $LHAPDF_PATH/share/LHAPDF/lhapdf.conf $workdir/LHAPDFsets/lhapdf.conf
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/NNPDF23_lo_as_0130_qed $workdir/LHAPDFsets/NNPDF23_lo_as_0130_qed
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/NNPDF30_nlo_as_0119 $workdir/LHAPDFsets/NNPDF30_nlo_as_0119
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/NNPDF30_nlo_as_0117 $workdir/LHAPDFsets/NNPDF30_nlo_as_0117  
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/PDF4LHC15_nlo_mc $workdir/LHAPDFsets/PDF4LHC15_nlo_mc
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/CT14nlo  $workdir/LHAPDFsets/CT14nlo 
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/MMHT2014nlo68clas118 $workdir/LHAPDFsets/MMHT2014nlo68clas118
ln -s /cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/NNPDF31_nnlo_as_0118 $workdir/LHAPDFsets/NNPDF31_nnlo_as_0118
export LHAPDF_DATA_PATH=$workdir/LHAPDFsets/

#download STXS Classification package
git clone ssh://git@gitlab.cern.ch:7999/LHCHIGGSXS/LHCHXSWG2/STXS/Classification.git
if [ -d Classification ]; then
    cd Classification
    source /cvmfs/sft.cern.ch/lcg/releases/MCGenerators/rivet/3.1.4-2f152/x86_64-centos7-gcc62-opt/rivetenv.sh
    sed -i 's/scale(hist, sf)/ /g' HiggsTemplateCrossSections.cc
    rivet-build RivetHiggsTemplateCrossSections.so HiggsTemplateCrossSections.cc
    export RIVET_ANALYSIS_PATH=$PWD
    cd $workdir
fi

#make setup script
echo '#!/bin/sh' > ./setup.sh
echo 'source /cvmfs/sft.cern.ch/lcg/views/LCG_93/x86_64-centos7-gcc62-opt/setup.sh' >> ./setup.sh
echo "export MGPATH=$workdir/MG5_aMC_v2_5_5/" >> ./setup.sh
echo "export PATH=$LHAPDF_PATH/bin/:\$PATH" >> ./setup.sh
echo "export LD_LIBRARY_PATH=$LHAPDF_PATH/lib/:\$LD_LIBRARY_PATH" >> ./setup.sh
echo "export PYTHONPATH=\`ls $LHAPDF_PATH/lib/python*/site-packages/ -d\`:\$PYTHONPATH" >> ./setup.sh
echo "export LHAPDF_DATA_PATH=$workdir/LHAPDFsets/" >> ./setup.sh
echo 'source /cvmfs/sft.cern.ch/lcg/releases/MCGenerators/rivet/3.1.4-2f152/x86_64-centos7-gcc62-opt/rivetenv.sh' >> ./setup.sh
echo "export RIVET_ANALYSIS_PATH=$workdir/Classification" >> ./setup.sh
source ./setup.sh

#download madgraph
wget https://launchpad.net/mg5amcnlo/2.0/2.5.x/+download/MG5_aMC_v2.5.5.tar.gz -O MG5_aMC_v2.5.5.tar.gz
tar xf MG5_aMC_v2.5.5.tar.gz
rm MG5_aMC_v2.5.5.tar.gz

#setup madgraph
cd MG5_aMC_v2_5_5/ 
echo "lhapdf = $LHAPDF_PATH/bin/lhapdf-config" >> input/mg5_configuration.txt
echo "automatic_html_opening = False" >> input/mg5_configuration.txt 
echo "cpp_compiler = /cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-centos7/bin/g++" >> input/mg5_configuration.txt 
echo "fortran_compiler = /cvmfs/sft.cern.ch/lcg/releases/gcc/6.2.0-b9934/x86_64-centos7/bin/gfortran" >> input/mg5_configuration.txt 
sed -i 's/# auto_update = 7/auto_update = 0/g' input/mg5_configuration.txt
sed -i 's/# run_mode = 2/run_mode = 0/g' input/mg5_configuration.txt

#install mg5 packages
#NOTE collier should not be used. In order to avoid that MG5 installs it automatically at the first event generation, we will manually disable it.
./bin/mg5_aMC <<EOD
install pythia8
install oneloop
install ninja
quit
EOD
echo "collier = None" >> input/mg5_configuration.txt


# mg5 2.5.5 is setup for an older lhapdf version than the one required by pythia8245 available in cmvfs 
# add patch in mg5 to fix missing includes of boost libraries
patch ./Template/LO/Source/PDF/pdf_lhapdf6.cc  ../LHAPDFpatch.txt
patch ./Template/NLO/Source/PDF/pdf_lhapdf6.cc  ../LHAPDFpatch.txt

cd ..

# we are currently using a local installation of pythia8 because of issues with the cmvfs installation
# we will properly source that  
export PYTHIA8DATA=$workdir/MG5_aMC_v2_5_5/HEPTools/pythia8//share/Pythia8/xmldoc
echo "export PYTHIA8DATA=$workdir/MG5_aMC_v2_5_5/HEPTools/pythia8//share/Pythia8/xmldoc" >> ./setup.sh

#download and extract the software for the kappa lambda NLO reweighting
wget --no-check-certificate https://cp3.irmp.ucl.ac.be/projects/madgraph/raw-attachment/wiki/HiggsSelfCoupling/trilinear-RW.tar.gz 
tar xf trilinear-RW.tar.gz
rm trilinear-RW.tar.gz

