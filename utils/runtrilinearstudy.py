#!/bin/python
import os,sys,copy,math
from lheCombiner import lheCombiner
from optparse import OptionParser
import filecmp

def run_dummy_process(mg5_dir,pdfset):
    print ("generating ttbar dummy process needed for LHAPDF lib")
    dummy_gen=[
        "#/bin/bash",
        "%s/bin/mg5_aMC <<EOF"%mg5_dir,
        "generate p p > t t~",
        "output %s/test_ttbar"%mg5_dir,
        "exit",
        "EOF",
        "sed -i 's/10000 = nevents/10 = nevents/g' %s/test_ttbar/Cards/run_card.dat"%mg5_dir,
        "sed -i 's/nn23lo1    = pdlabel/lhapdf = pdlabel/g' %s/test_ttbar/Cards/run_card.dat"%mg5_dir,
        "sed -i 's/230000    = lhaid/"+str(pdfset)+"   = lhaid/g' %s/test_ttbar/Cards/run_card.dat"%mg5_dir,
        "/%s/test_ttbar/bin/generate_events<< EOF"%mg5_dir,
        "shower=PYTHIA8",
        "detector=OFF",
        "analysis=OFF",
        "madspin=OFF",
        "reweight=OFF",
        "EOF",
    ]  
    dummy_script = open("rundummy.sh","w")
    dummy_script.write('\n'.join(dummy_gen))
    dummy_script.close()
    os.system("chmod u+x rundummy.sh")
    os.system("./rundummy.sh")
    print ("end generation ttbar dummy process")
    return mg5_dir+"/test_ttbar"


scale_dict={
    "ZH":  108.0938,
    "WH":  102.6925,
    "ttH": 235.0,
    "tHq": 148.75
}   


def runtrilinearstudy(proc,nevents,mg5_dir,trilinear_dir,store_dir,pdfset=90500,seed=0,tune="mg5",dummy_process_dir="",clean=False, pdfsyst=False):


    process_dict={
        "ZH":  ["p p > h z [LOonly= QCD]"],
        "WH":  ["p p > h w+ [LOonly= QCD]","p p > h w- [LOonly= QCD]"],
        "ttH": ["p p > t t~ h [LOonly= QCD]"],
        # "tHq": ["p p > h t j \$\$ w+ w- [LOonly= QCD]", "p p > h t~ j \$\$ w+ w- [LOonly= QCD]"],
        "Hjj" : ["p p > h j j [LOonly= QCD]"]
    }
    
    LHAPDF_dir=mg5_dir+"/HEPTools/lhapdf6/share/LHAPDF/"

    if not os.path.exists("%s/models/hhh-model"%mg5_dir):
        os.system("cp -r %s/hhh-model %s/models/"%(trilinear_dir,mg5_dir))
    os.system("mkdir %s"%(store_dir))
    os.chdir("%s"%(store_dir))
    store_dir=os.getcwd()
    #if 'clean' only the relevant output will be copied in the end to the store_dir
    if clean:
        os.system("mkdir workdir")
        work_dir = os.getcwd()+"/workdir/"
        print "I will run in "+work_dir
        os.chdir("%s"%(work_dir))
        work_dir=os.getcwd()
    else:
        work_dir=store_dir
    os.system("cp %s/gevirt.sh %s/"%(trilinear_dir,work_dir))
    # # # generate dummy process
    # # ####################################################
    # # ####################################################
    if dummy_process_dir=="": dummy_process_dir=mg5_dir+"/test_ttbar"
    if not (os.path.isdir(dummy_process_dir) and os.path.isdir(dummy_process_dir+"/lib/Pdfdata") and os.path.isdir(dummy_process_dir+"/lib/PDFsets")) :
        dummy_process_dir=run_dummy_process(mg5_dir,str(pdfset).split(",")[0])
    # # # generating LO and NLO events 
    # # ####################################################
    # # ####################################################
    mg5script = open("runmg5MC.sh","w")
    mg5script.write("%s/bin/mg5_aMC <<EOF\n"%mg5_dir)
    mg5script.write("import model hhh-model\n")
    if proc=="Hjj":
        mg5script.write("set complex_mass_scheme\n")
    mg5script.write("generate %s\n"%process_dict[proc][0])
    if len(process_dict[proc])>1:
        for iproc in range(1,len(process_dict[proc])):
            mg5script.write("add  process %s\n"%process_dict[proc][iproc])
    mg5script.write("output %s/C1_MC\n"%(work_dir))
    mg5script.write("quit\n")
    mg5script.write("EOF\n")
    mg5script.close()

    os.system("chmod u+x runmg5MC.sh")
    print ("generating born diagrams - see  %s/runmg5MC.log for log"%(store_dir))
    os.system("./runmg5MC.sh >  %s/runmg5MC.log"%(store_dir))
    print ("end generation born diagrams")
    os.system("./gevirt.sh C1_MC")
    os.system("echo '%s/bin/mg5_aMC <<EOF' > runmg5ME.sh"%mg5_dir)
    os.system("echo 'import model hhh-model' >> runmg5ME.sh")
    if proc=="Hjj":
        os.system("echo 'set complex_mass_scheme' >> runmg5ME.sh")
    os.system("cat proc_ml >> runmg5ME.sh")
    os.system("echo 'output %s/C1_ME' >> runmg5ME.sh"%(work_dir))
    os.system("echo EOF >> runmg5ME.sh")
    os.system("cp %s/madgraph/loop/loop_diagram_generation.py %s/loop_diagram_generation_original.py"%(mg5_dir,trilinear_dir))

    if proc == 'ttH':
        loopfile = "%s/tth-loop_diagram_generation.py"%trilinear_dir
    else:
        loopfile = "%s/vvh-loop_diagram_generation.py"%trilinear_dir
    if not filecmp.cmp(loopfile,"%s/madgraph/loop/loop_diagram_generation.py"%mg5_dir,shallow=False):
        os.system("cp %s %s/madgraph/loop/loop_diagram_generation.py"%(loopfile,mg5_dir))

    os.system("chmod u+x runmg5ME.sh")
    print ("generating NLO EW diagrams - see  %s/runmg5ME.log for log"%(store_dir))
    os.system("./runmg5ME.sh >  %s/runmg5ME.log"%(store_dir))
    print ("end generation NLO EW diagrams")
    os.system("cp %s/makefile ./C1_ME/SubProcesses/"%(trilinear_dir))
    os.system("cp %s/check_OLP.f ./C1_ME/SubProcesses/"%(trilinear_dir))
    os.system("sed -i 's/lhaid=90500/lhaid=%s/g' C1_ME/SubProcesses/check_OLP.f"%(str(pdfset).split(",")[0]))
    os.system("cp ./check_olp.inc ./C1_ME/SubProcesses/")
    os.system("cp ./C1_ME/SubProcesses/P0_*/pmass.inc ./C1_ME/SubProcesses/")
    os.system("cp ./C1_ME/SubProcesses/P0_*/nsqso_born.inc ./C1_ME/SubProcesses/")    
    os.system("cp ./C1_ME/SubProcesses/P0_*/nsquaredSO.inc ./C1_ME/SubProcesses/")
    os.system("cp ./C1_MC/SubProcesses/c_weight.inc ./C1_ME/SubProcesses/")
    os.system("cp ./C1_MC/SubProcesses/P0_*/nexternal.inc ./C1_ME/SubProcesses/")    
    os.system("cp %s/lib/libpdf.a ./C1_ME/lib/"%(dummy_process_dir))
    if os.path.isfile("%s/lib/libLHAPDF.a"%dummy_process_dir):
        os.system("cp %s/lib/libLHAPDF.a ./C1_ME/lib/"%(dummy_process_dir))
    elif os.path.isfile("%s/lib/libLHAPDF.a"%LHAPDF_dir):
        os.system("cp %s/lib/libLHAPDF.a ./%C1_ME/lib/"%(LHAPDF_dir))
    else:
        os.system("cp %s/src/.libs/libLHAPDF.a ./C1_ME/lib/"%(LHAPDF_dir))
    os.system("cp -r %s/lib/Pdfdata ./C1_ME/lib/"%(dummy_process_dir))
    os.system("cp -r %s/lib/PDFsets ./C1_ME/lib/"%(dummy_process_dir))
    os.chdir("C1_ME/SubProcesses/")
    print ("compiling reweight machinery - see  %s/makefile.log for log"%(store_dir))
    os.system("make OLP_static >  %s/makefile.log"%(store_dir))
    os.system("make check_OLP >>  %s/makefile.log"%(store_dir))
    os.chdir(work_dir)
    print ("end compilation")
    runcardfile = open("C1_MC/Cards/run_card.dat", "rt")
    runcardcontent = runcardfile.read()
    runcardcontent = runcardcontent.replace('nn23nlo = pdlabel', 'lhapdf = pdlabel')
    runcardcontent = runcardcontent.replace('244600  = lhaid', '%s  = lhaid'%str(pdfset))
    # runcardcontent = runcardcontent.replace('False    = fixed_ren_scale', 'True    = fixed_ren_scale')
    # runcardcontent = runcardcontent.replace('False    = fixed_fac_scale', 'True    = fixed_fac_scale')
    runcardcontent = runcardcontent.replace('0    = iseed','%s    = iseed'%str(seed))
    # runcardcontent = runcardcontent.replace('91.118   = muR_ref_fixed', '%.4f   = muR_ref_fixed'%scale_dict[proc])
    # runcardcontent = runcardcontent.replace('91.118   = muF_ref_fixed', '%.4f   = muF_ref_fixed'%scale_dict[proc])
    runcardcontent = runcardcontent.replace('False = store_rwgt_inf', 'True = store_rwgt_inf')
    runcardcontent = runcardcontent.replace('-1.0 = req_acc', '0.00001 = req_acc')
    runcardcontent = runcardcontent.replace('10000 = nevents', '%i = nevents'%nevents)
    runcardcontent = runcardcontent.replace('HERWIG6   = parton_shower', 'PYTHIA8   = parton_shower')
    if pdfsyst:
        rw_pdf="True"
        for i in range(len(str(pdfset).split(","))-1):
            rw_pdf=rw_pdf+",False"
        runcardcontent = runcardcontent.replace('False = reweight_PDF', '%s   = reweight_PDF'%rw_pdf)
    runcardfile.close()
    runcardfile = open("C1_MC/Cards/run_card.dat", "wt")
    runcardfile.write(runcardcontent)
    runcardfile.close()

    evgeneratescript = open("generateMC.sh","w")
    evgeneratescript.write("./C1_MC/bin/generate_events << EOF\n")
    evgeneratescript.write("order=LO\n")
    evgeneratescript.write("fixed_order=OFF\n")
    evgeneratescript.write("shower=OFF\n")
    evgeneratescript.write("madspin=OFF\n")
    evgeneratescript.write("reweight=OFF\n")
    evgeneratescript.write("done\n")
    evgeneratescript.write("done\n")
    evgeneratescript.write("EOF\n")
    evgeneratescript.close()
    os.system("chmod u+x generateMC.sh")
    print ("generating LO MC events - see  %s/generateMC.log for log"%(store_dir))
    os.system("./generateMC.sh >  %s/generateMC.log"%(store_dir))
    os.system("gzip -d C1_MC/Events/run_01_LO/events.lhe.gz")
    #os.system("sed -i 's/    2 1.166379e-05 # Gf/    1 1.325070e+02 # aEWM1\\n    2 1.166379e-05 # Gf/g' C1_MC/Events/run_01_LO/events.lhe")
    os.system("cp C1_MC/Events/run_01_LO/events.lhe C1_ME/SubProcesses/")
    os.chdir("C1_ME/SubProcesses/")
    print ("end generation LO MC events")
    print ("running reweight machinery - see  %s/check_OLP.log for log"%(store_dir))
    os.system("./check_OLP >  %s/check_OLP.log"%(store_dir))
    with open("%s/check_OLP.log"%(store_dir), "r") as fp:
        LO=float([line for line in fp if "SUM OF ORIGINAL WEIGHTS" in line][0].split(":")[1])
    with open("%s/check_OLP.log"%(store_dir), "r") as fp:   
        NLO=float([line for line in fp if "SUM OF NEW WEIGHTS" in line][0].split(":")[1])
    print ("end reweighting")
    print ("combining LO and NLO LHE files and saving in %s/events_LO_NLO.lhe"%(work_dir))
    lheCombiner("%s/C1_ME/SubProcesses/events.lhe"%(work_dir),"%s/C1_ME/SubProcesses/events_rwgt.lhe"%(work_dir),"%s/events_LO_NLO.lhe"%(work_dir))
    # os.system("cp %s/loop_diagram_generation_original.py %s/madgraph/loop/loop_diagram_generation.py"%(trilinear_dir,mg5_dir))
    os.chdir(work_dir)
    # run the shower over merged LHE file
    ####################################################
    ####################################################
    os.system("cp %s/events_LO_NLO.lhe  C1_MC/Events/run_01_LO/events.lhe"%(work_dir))
    print ("showering events - see  %s/shower.log for log"%(work_dir))
    os.chdir("C1_MC/Events/run_01_LO")
    run_pythia8=open("run_pythia8.sh","w")
    run_pythia8.write("#!/usr/bin/env bash\nLD_LIBRARY_PATH=%s/HEPTools/lib:$LD_LIBRARY_PATH %s/HEPTools/MG5aMC_PY8_interface/MG5aMC_PY8_interface pythia8.cmd"%(mg5_dir,mg5_dir))
    run_pythia8.close()
    os.system("chmod u+x run_pythia8.sh")
    if tune=="atlas_A14NNPDF23LO":
        atlas_A14NNPDF23LO_tune=[
            "Main:timesAllowErrors = 500",
            "Main:numberOfEvents      = -1",
            "Random:setSeed = on",
            "Random:seed = %s"%str(seed),
            "6:m0 = 172.5",
            "23:m0 = 91.1876",
            "23:mWidth = 2.4952",
            "24:m0 = 80.399",
            "24:mWidth = 2.085",
            "StandardModel:sin2thetaW = 0.23113",
            "StandardModel:sin2thetaWbar = 0.23146",
            "ParticleDecays:limitTau0 = on",
            "ParticleDecays:tau0Max = 10.0",
            "Tune:ee = 7", 
            "Tune:pp = 14",
            "SpaceShower:rapidityOrder = on",
            "SigmaProcess:alphaSvalue = 0.140",
            "SpaceShower:pT0Ref = 1.56",
            "SpaceShower:pTmaxFudge = 0.91",
            "SpaceShower:pTdampFudge = 1.05",
            "SpaceShower:alphaSvalue = 0.127",
            "TimeShower:alphaSvalue = 0.127",
            "BeamRemnants:primordialKThard = 1.88",
            "MultipartonInteractions:pT0Ref = 2.09",
            "MultipartonInteractions:alphaSvalue = 0.126",
            "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
            "ColourReconnection:range = 1.71",
            "HEPMCoutput:file         = pythia8_events.hepmc",
            "Beams:LHEF=events.lhe",
            "Beams:frameType=4",
            "LHEFInputs:nSubruns=1",
            "Main:subrun=0",
        ]
        pythia8=open("pythia8.cmd","w")
        pythia8.write('\n'.join(atlas_A14NNPDF23LO_tune)+'\n')
        pythia8.close()  

    if tune=="cms_CP5":
        cms_CP5_tune=[
            "Main:numberOfEvents      = -1",
            "HEPMCoutput:file         = pythia8_events.hepmc",
            "Random:setSeed = on",
            "Random:seed = %s"%str(seed),
            "SLHA:minMassSM=1000.",
            "SpaceShower:alphaSorder=2",
            "SpaceShower:alphaSvalue=1.1800000000e-01",
            "MultipartonInteractions:coreFraction=0.63",
            "MultipartonInteractions:coreRadius=0.7634",
            "MultipartonInteractions:bProfile=2",
            "MultipartonInteractions:alphaSorder=2",
            "MultipartonInteractions:alphaSvalue=0.118",
            "MultipartonInteractions:ecmPow=0.03344",
            "MultipartonInteractions:pT0Ref=1.41",
            "Beams:setProductionScalesFromLHEF=off",
            "SigmaProcess:alphaSvalue=0.118",
            "SigmaProcess:alphaSorder=2",
            "Check:epTolErr=1.0000000000e-02",
            "SigmaTotal:zeroAXB=off",
            "SigmaTotal:sigmaTot=100.309",
            "SigmaTotal:mode=0",
            "SigmaTotal:sigmaEl=21.89",
            "ParticleDecays:limitTau0=on",
            "ParticleDecays:allowPhotonRadiation=on",
            "ParticleDecays:tau0Max=10",
            "ColourReconnection:range=5.176",
            "PDF:pSet=LHAPDF6:NNPDF31_nnlo_as_0118",
            "Main:timesAllowErrors=10000",
            "TimeShower:alphaSvalue=1.1800000000e-01",
            "TimeShower:alphaSorder=2",
            "Tune:ee=7",
            "Tune:preferLHAPDF=2",
            "Tune:pp=14",
            "Beams:frameType=4",
            "JetMatching:setMad=off",
            "JetMatching:etaJetMax=1.0000000000e+03",
            "HEPMCoutput:scaling=1.0000000000e+09",
            "LHEFInputs:nSubruns=1",
            "Main:subrun=0",
            "Beams:LHEF=events.lhe"
        ]
        pythia8=open("pythia8.cmd","w")
        pythia8.write('\n'.join(cms_CP5_tune)+'\n')
        pythia8.close()  

    if tune=="mg5":
        mg5_tune=[
            "Main:numberOfEvents      = -1",
            "HEPMCoutput:file         = pythia8_events.hepmc",
            "Random:setSeed = on",
            "Random:seed = %s"%str(seed),
            "SysCalc:fullCutVariation = off",
            "Beams:frameType=4",
            "JetMatching:setMad=off",
            "JetMatching:etaJetMax=1.0000000000e+03",
            "HEPMCoutput:scaling=1.0000000000e+09",
            "Check:epTolErr=1.0000000000e-02",
            "LHEFInputs:nSubruns=1",
            "Main:subrun=0",
            "Beams:LHEF=events.lhe",
        ]
        pythia8=open("pythia8.cmd","w")
        pythia8.write('\n'.join(mg5_tune)+'\n')  
        pythia8.close()    
    if proc=="ZH":
        os.system("echo '23:onMode = off' >>  pythia8.cmd") # switch OFF all Z decay channels
        os.system("echo '23:onIfAny = 11 12 13 14 15 16' >>  pythia8.cmd") # only leptonic decay
    if proc=="WH":
        os.system("echo '24:onMode = off' >>  pythia8.cmd") # switch OFF all W decay channels
        os.system("echo '24:onIfAny = 11 12 13 14 15 16' >>  pythia8.cmd") # only leptonic decay 
    os.environ['PYTHIA8DATA']=mg5_dir+"/HEPTools/pythia8/share/Pythia8/xmldoc"
    os.system("./run_pythia8.sh>  %s/shower.log"%(store_dir))
    print ("end showering")
    # # # run rivet
    # # ####################################################
    # # ####################################################
    os.chdir("%s/"%(work_dir))
    if "VBF" in proc or "Hjj" in proc: mode="VBF"
    elif "ZH" in proc: mode="QQ2ZH"
    elif "WH" in proc: mode="WH"
    elif "ttH" in proc: mode="TTH"
    os.environ['HIGGSPRODMODE']=mode
    print("running Rivet routine - see  %s/rivet.log for log"%(store_dir))
    os.system("rivet -v --analysis HiggsTemplateCrossSections %s/C1_MC/Events/run_01_LO/pythia8_events.hepmc >%s/rivet.log"%(work_dir,store_dir))
    ########################################################
    print ("inclusive %s C1:"%proc, round(NLO/LO,5))

    if clean:
        print ("removing all the auxiliary folders")
        os.system("rm -rf C1_MC C1_ME *.lhe")
        os.system("cp %s/* %s/"%(work_dir,store_dir))

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--proc", help="Process (Hjj, ZH, WH, ttH, or tHq)")
    parser.add_option("--nevents", type="int",  default=1000,       help="N events to generate")
    parser.add_option("--dummy_process_dir", default="", help="Example mg5 generation from which we will copy some data and lib")
    parser.add_option("--trilinear_dir", help="Folder containing trilinear rew. code")
    parser.add_option("--mg5_dir",  help="MG5 folder")
    parser.add_option("--store_dir",  help="folder to store the output generated events")
    parser.add_option("--pdfset",  default=90500, help="pdfset to be used: 90500,131000,25000")
    parser.add_option("--seed",  default=0, help="generation seed")
    parser.add_option("--tune",  default="mg5", help="shower tune: mg5, atlas_A14NNPDF23LO, cms")
    parser.add_option("--clean",  default=False, help="remove all the folders used for the calculation", action="store_true")
    parser.add_option("--pdfsyst",  default=False, help="add nominal pdf internal variations", action="store_true")
    (options,args)=parser.parse_args()

    runtrilinearstudy(options.proc,options.nevents,options.mg5_dir,options.trilinear_dir,options.store_dir,options.pdfset,options.seed,options.tune,options.dummy_process_dir, options.clean,options.pdfsyst)

