#!/usr/bin/python
import os
import math
from optparse import OptionParser

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--input",  help="path to text")
    parser.add_option("--output", help="output file")
    (options,args)=parser.parse_args()

    with open(options.output, 'w') as fout:

        fout.write('\\begin{tabular}{|l | r r r r|}\n')
        fout.write("\\hline\n")
        fout.write('STXS BIN & $\Cone\cdot 10^{2}$ & $\sigma$ stat. & $\sigma$ scale & $\sigma$ pdf \\\\ \n')
        fout.write("\\hline\n")

        with open(options.input, 'r') as f:
            for line in f:
                if "=" in line:
                    line = line.replace('\n','')
                    bins_name = line.split("=")[0]
                    nominal = float(line.split("=")[1].split("+/-")[0])
                    stat = float(line.split("=")[1].split("+/-")[1])
                    scale = float(line.split("=")[1].split("+/-")[2])
                    pdf = float(line.split("=")[1].split("+/-")[3])
                    total = math.sqrt(stat**2+scale**2+pdf**2)
                    systtotal = math.sqrt(scale**2+pdf**2)
                    bins_name = bins_name.replace("_","\_")
                    bins_name = "$\mathrm{"+bins_name+"}$"
                    outline = bins_name+" & "+str(nominal)+" & "+str(stat)+" & "+str(scale)+" & "+str(pdf)+' \\\\\n'
                    fout.write(outline)

        fout.write("\hline\n")
        fout.write("\end{tabular}\n")
