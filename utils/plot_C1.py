#!/usr/bin/python
import math
import os
import glob
from optparse import OptionParser
import ROOT

ROOT.gROOT.SetBatch(True)

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--input",  help="path to text")
    parser.add_option("--output", help="output directory")
    parser.add_option("--proc", help="process")
    parser.add_option("--pdfset", default=False,  action="store_true")
    parser.add_option("--altpdfset", default=False,  action="store_true")
    parser.add_option("--yratiomin", type="float", default=0.95)
    parser.add_option("--yratiomax", type="float", default=1.05)
    (options,args)=parser.parse_args()
    ROOT.gROOT.SetStyle("ATLAS")

    bins_name=[]
    index=1
    nbins=sum(1 for line in open(options.input) if not ("UNKNOWN" in line))-2
    nominal=ROOT.TH1F("h","h",nbins,0,nbins)
    scale_up=ROOT.TH1F("h","h",nbins,0,nbins)
    pdf_up=ROOT.TH1F("h","h",nbins,0,nbins)
    scale_down=ROOT.TH1F("h","h",nbins,0,nbins)
    pdf_down=ROOT.TH1F("h","h",nbins,0,nbins)
    stat_up=ROOT.TH1F("h","h",nbins,0,nbins)
    stat_down=ROOT.TH1F("h","h",nbins,0,nbins)
    scale_up_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    pdf_up_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    scale_down_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    pdf_down_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    stat_up_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    stat_down_rel=ROOT.TH1F("h","h",nbins,0,nbins)
    with open(options.input, 'r') as f:
        for line in f:
            if "=" in line:
                if "UNKNOWN" in line.split("=")[0]: continue                
                bin_name = line.split("=")[0]
                if options.proc=="Hjj":
                    bin_name = bin_name.replace("QQ2HQQ_","")
                bins_name.append(bin_name)
                nominal.SetBinContent(index,float(line.split("=")[1].split("+/-")[0]))
                c1=float(line.split("=")[1].split("+/-")[0])
                stat_up.SetBinContent(index,c1+abs(float(line.split("=")[1].split("+/-")[1])))
                scale_up.SetBinContent(index,c1+abs(float(line.split("=")[1].split("+/-")[2])))
                pdf_up.SetBinContent(index,c1+abs(float(line.split("=")[1].split("+/-")[3])))
                stat_down.SetBinContent(index,c1-abs(float(line.split("=")[1].split("+/-")[1])))
                scale_down.SetBinContent(index,c1-abs(float(line.split("=")[1].split("+/-")[2])))
                pdf_down.SetBinContent(index,c1-abs(float(line.split("=")[1].split("+/-")[3])))
                stat_up_rel.SetBinContent(index,(c1+abs(float(line.split("=")[1].split("+/-")[1])))/c1)
                scale_up_rel.SetBinContent(index,(c1+abs(float(line.split("=")[1].split("+/-")[2])))/c1)
                pdf_up_rel.SetBinContent(index,(c1+abs(float(line.split("=")[1].split("+/-")[3])))/c1)
                stat_down_rel.SetBinContent(index,(c1-abs(float(line.split("=")[1].split("+/-")[1])))/c1)
                scale_down_rel.SetBinContent(index,(c1-abs(float(line.split("=")[1].split("+/-")[2])))/c1)
                pdf_down_rel.SetBinContent(index,(c1-abs(float(line.split("=")[1].split("+/-")[3])))/c1)                
                total_error=math.sqrt(float(line.split("=")[1].split("+/-")[1])**2+float(line.split("=")[1].split("+/-")[2])**2+float(line.split("=")[1].split("+/-")[3])**2)
                nominal.SetBinError(index,total_error)
                if index==2: max_y=c1
                min_y=c1
                index+=1
    print(min_y,max_y)
    frame=ROOT.TH1F("frame",'frame',index-1,0,index-1)
    c1=ROOT.TCanvas("c","c",800,800)
    canvas_mem=[]
    if options.proc=="Hjj":
        r = 0.67
    else:
        r = 0.5
    epsilon = 0.04
    pad_bottom = ROOT.TPad("pad_bottom", "pad_bottom", 0, 0.0, 1, r * (1 - epsilon))
    canvas_mem.append(pad_bottom)
    pad_bottom.SetFrameFillStyle(4000)
    pad_top = ROOT.TPad("pad_top", "pad_top", 0, r - epsilon, 1, 1)
    canvas_mem.append(pad_top)
    pad_top.SetFrameFillStyle(4000)
    pad_top.SetMargin(0.15, 0.03, epsilon, 0.04)  # l r b t
    pad_bottom.SetMargin(0.15, 0.03, 0.3, 0.0)
    pad_top.SetBottomMargin(epsilon)
    pad_bottom.SetTopMargin(0)
    pad_bottom.SetFillColor(0)
    pad_bottom.SetFrameFillStyle(0)
    pad_bottom.SetFillStyle(0)
    if options.proc=="Hjj":
        pad_bottom.SetBottomMargin(0.7)
    else:
        pad_bottom.SetBottomMargin(0.6)
    #pad_bottom.SetBottomMargin(0.60)
    pad_top.SetFillColor(0)
    pad_top.SetFrameFillStyle(0)
    pad_top.SetFillStyle(0)
    c1.cd()
    pad_bottom.Draw()
    c1.cd()
    pad_top.Draw()
    pad_top.cd()
    offset_y = 1.0
    frame.GetYaxis().SetTitle ("C1 #times 10^{2}")
    if min_y<0: frame.GetYaxis().SetRangeUser(min_y*1.5, max_y*1.5)
    else: frame.GetYaxis().SetRangeUser(0, max_y*2)

    if options.proc=="Hjj":
        frame.GetYaxis().SetLabelSize(0.051*0.67/0.5)
        frame.GetYaxis().SetTitleSize(0.06*0.67/0.5)
        frame.GetYaxis().SetTitleOffset(1.3 * 0.043/0.06  * offset_y * 0.5/0.67)
    else:
        frame.GetYaxis().SetLabelSize(0.051)
        frame.GetYaxis().SetTitleSize(0.06)
        frame.GetYaxis().SetTitleOffset(1.3 * 0.043/0.06 * offset_y)

    frame.GetYaxis().ChangeLabel(1, -1, -1, -1, -1, -1, " ") # Avoid the first label (0) to be clipped
    frame.GetXaxis().SetLabelSize(0.)
    frame.GetXaxis().SetTitleSize(0.)
    frame.Draw("Axis")
    scale_up.SetLineColor(ROOT.kRed)
    pdf_up.SetLineColor(ROOT.kOrange)
    scale_down.SetLineColor(ROOT.kRed)
    pdf_down.SetLineColor(ROOT.kOrange)
    stat_up.SetLineColor(ROOT.kBlue)
    stat_down.SetLineColor(ROOT.kBlue)
    scale_up.SetMarkerSize(0)
    pdf_up.SetMarkerSize(0)
    stat_up.SetMarkerSize(0)
    scale_down.SetMarkerSize(0)
    pdf_down.SetMarkerSize(0)
    stat_down.SetMarkerSize(0)
    # scale_up.SetLineWidth(2)
    # pdf_up.SetLineWidth(2)
    # stat_up.SetLineWidth(2)
    # scale_down.SetLineWidth(2)
    # pdf_down.SetLineWidth(2)
    # stat_down.SetLineWidth(2)
    scale_up.Draw("HIST same")
    pdf_up.Draw("HIST same")
    stat_up.Draw("HIST same")
    scale_down.Draw("HIST same")
    pdf_down.Draw("HIST same")
    stat_down.Draw("HIST same")
    nominal.Draw("HIST same")
    legend = ROOT.TLegend(0.75, 0.60, 0.90, 0.90)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetHeader(options.proc)
    legend.AddEntry(nominal, "nominal C1", "L" )
    legend.AddEntry(stat_up, "stat. unc.", "L" )
    legend.AddEntry(scale_up, "scale unc.", "L" )
    legend.AddEntry(pdf_up, "pdf unc.", "L" )     
    legend.Draw()

    c1.cd()
    pad_bottom.cd()
    frame2=ROOT.TH1F("frame2",'frame2',index-1,0,index-1)
    for iname,name in enumerate(bins_name):
        frame2.GetXaxis().SetBinLabel(iname+1,name)
    frame2.GetXaxis().LabelsOption("v")
    frame2.GetYaxis().SetRangeUser(options.yratiomin,options.yratiomax)
    if options.proc=="Hjj":
        frame2.GetYaxis().SetLabelSize(0.05*0.5/0.67)
        frame2.GetYaxis().SetTitleSize(0.058*0.5/0.67)
        frame2.GetYaxis().SetTitleOffset(1.3* 0.043/0.06 *0.67/0.5)
        frame2.GetXaxis().SetTitleOffset(1.25)
        frame2.GetXaxis().SetLabelSize(0.045)
        frame2.GetYaxis().SetTitleFont(42)
    else:
        frame2.GetYaxis().SetLabelSize(0.05)
        frame2.GetYaxis().SetTitleSize(0.058)
        frame2.GetYaxis().SetTitleOffset(1.3* 0.043/0.06)
        frame2.GetXaxis().SetTitleOffset(1.25)
        #frame2.GetXaxis().SetLabelSize(0.045 * 0.67/0.5)
        frame2.GetXaxis().SetLabelSize(0.06)
        frame2.GetYaxis().SetTitleFont(42)

    frame2.GetYaxis().ChangeLabel(-1, -1, -1, -1, -1, -1, " ") # Avoid the first label (0) to be clipped
    frame2.GetXaxis().SetTitleSize(0.10)
    frame2.GetYaxis().CenterTitle(True)
    frame2.GetYaxis().SetTitle("#frac{#pm1#sigma variation}{nominal}")
    frame2.Draw("Axis")
    scale_up_rel.SetLineColor(ROOT.kRed)
    pdf_up_rel.SetLineColor(ROOT.kOrange)
    scale_down_rel.SetLineColor(ROOT.kRed)
    pdf_down_rel.SetLineColor(ROOT.kOrange)
    stat_up_rel.SetLineColor(ROOT.kBlue)
    stat_down_rel.SetLineColor(ROOT.kBlue)
    # scale_up_rel.SetLineWidth(2)
    # pdf_up_rel.SetLineWidth(2)
    # stat_up_rel.SetLineWidth(2)
    # scale_down_rel.SetLineWidth(2)
    # pdf_down_rel.SetLineWidth(2)
    # stat_down_rel.SetLineWidth(2)
    scale_up_rel.SetMarkerSize(0)
    pdf_up_rel.SetMarkerSize(0)
    stat_up_rel.SetMarkerSize(0)
    scale_down_rel.SetMarkerSize(0)
    pdf_down_rel.SetMarkerSize(0)
    stat_down_rel.SetMarkerSize(0)
    scale_up_rel.Draw("HIST same")
    pdf_up_rel.Draw("HIST same")
    stat_up_rel.Draw("HIST same")
    scale_down_rel.Draw("HIST same")
    pdf_down_rel.Draw("HIST same")
    stat_down_rel.Draw("HIST same")
    tline=ROOT.TLine(0,1.,index-1,1.)
    tline.SetLineStyle(7)
    tline.Draw("same")

    c1.SaveAs(options.output)
  
