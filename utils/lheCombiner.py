#!/usr/bin/python
from optparse import OptionParser
import os,sys
import argparse
import logging
import xml.etree.ElementTree as ET
import xml.dom.minidom
# from xml.etree.ElementTree import Element, SubElement, Comment, tostring



def lheCombiner(inputLHE, inputRW, outputFile):

    if not os.path.exists("%s" % inputLHE):
        logging.error("LO lhe file doesn't exit. Note the LO lhe file must be named 'events.lhe' and put under the input directory! Abort.")
        exit
    if not os.path.exists("%s" % inputRW):
        logging.error("NLO lhe file doesn't exit. Note the NLO lhe file must be named 'events_rwgt.lhe' and put under the input directory! Abort.")
        exit

    logging.info("Reading LO lhe file...")
    lhe_lo = ET.parse("%s" % (inputLHE))
    logging.info("Reading reweighted lhe file...")
    lhe_nlo = ET.parse("%s" % (inputRW))
    
    lhe_lo_events = lhe_lo.getroot()
    lhe_nlo_events = lhe_nlo.getroot()
    
    logging.info("Adding nlo kappa reweight into lo lhe...")

    #check for last weight id used
    wgt=lhe_lo_events.findall("./event[1]/rwgt/wgt")
    id_max=-1
    for w in wgt:
        if id_max<int(w.attrib['id']):
            id_max=int(w.attrib['id'])
    index_w=1
    
    #addign a NLO weight for each weight variations
    reweight_group = lhe_lo_events.find('header').find('initrwgt')
    weight_group_pdf=[]
    for group in reweight_group:
        if "scale_variation" in group.attrib['name']:
            old_weight_text_list=[]
            for w in  group:
                if "-1 muR" in w.text:
                    old_weight_text_list.append((w.text).replace("-1 muR","-1 kl muR"))
                else: old_weight_text_list.append((w.text).replace("0 muR","0 kl muR"))
            for t in  old_weight_text_list:
                weight_name = ET.SubElement(group, 'weight')
                weight_name.attrib = {'id': '%s'%str(id_max+index_w)}
                weight_name.text = t
                index_w+=1
        if "PDF_variation" in group.attrib['name']:
            weight_group_pdf.append(group)
            old_weight_text_list=[]
            for w in  group:
                old_weight_text_list.append(w.text.replace((w.text).split()[2],(w.text).split()[2]+"_kl"))
            for t in  old_weight_text_list:
                weight_name = ET.SubElement(group, 'weight')
                weight_name.attrib = {'id': '%s'%str(id_max+index_w)}
                weight_name.text = t   
                index_w+=1
    if len(weight_group_pdf)>0:
        for ig in range(1,len(weight_group_pdf)):
            for el in weight_group_pdf[ig]:
                weight_group_pdf[0].append(el)
            reweight_group.remove(weight_group_pdf[ig])


    for i_evt, event in enumerate(lhe_lo_events):
        if event.tag == "event":
            
            try:
                kappa_event = lhe_nlo_events[i_evt].text
            except (IndexError, KeyError, ValueError):
                logging.error("Event numbers in lo and nlo files don't match. Check your MG5 generation setup! Abort.")
                exit
            match=[i for i, j in zip(kappa_event.split()[:6], event.text.split()[:6]) if i == j]
            if len(match)==5:
                kappa_weight = float(kappa_event.split()[2])
                weight_nominal = float (event.text.split()[2])
                rwgt_element = event.find('rwgt')
                weight_list=[]
                for iw,w in  enumerate(rwgt_element):
                    weight_list.append(float(w.text))
                
                for iw,w in enumerate(weight_list):
                    weight = ET.SubElement(rwgt_element, 'wgt')
                    weight.attrib = {'id': '%s'%str(id_max+1+iw)}
                    weight.text = " %s " % (kappa_weight/weight_nominal*w)
            else:
                logging.error("Event numbers in lo and nlo files are misaligned! Abort")
                exit
            
            if i_evt % 1000 == 0:
                    logging.info("%s events processed." % str(i_evt))



    # pretty_xml = [line for line in xml.dom.minidom.parseString(ET.tostring(lhe_lo_events)).toprettyxml(indent=' ').split('\n') if line.strip()][1:]
    # pretty_print = '\n'.join(pretty_xml)

    logging.info("Writing combined lhe file to %s..." % (outputFile)) 
    # out_xml = open("%s" % (outputFile), "w")
    # out_xml.write(pretty_print)
    # out_xml.close()
    lhe_lo.write("%s" % (outputFile))

                
    
if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--inputDir",                 default="",         help="folder containing LO and NLO kappa reweight lhe files.")
    parser.add_option("--outName",                default="",         help="output combined LHE file")
    (options,args)=parser.parse_args()
    

    lheCombiner(options.inputDir, options.outName)











