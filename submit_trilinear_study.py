#!/bin/python
import os,sys,copy,math
from utils.runtrilinearstudy import runtrilinearstudy
from utils.runtrilinearstudy import run_dummy_process
from optparse import OptionParser


workdir=os.getcwd()
parser = OptionParser()
parser.add_option("--proc", help="Process (Hjj, ZH, WH, ttH, or tHq)")
parser.add_option("--nevents", type="int",  default=1000,   help="N events to generate")
parser.add_option("--dummy_process_dir", default="", help="Example mg5 generation from which we will copy some data and lib (absolute path)")
parser.add_option("--trilinear_dir", default=workdir+"/trilinear-RW/",help="Folder containing trilinear rew. code (absolute path)")
parser.add_option("--mg5_dir",  default=workdir+"/MG5_aMC_v2_5_5/", help="MG5 folder (absolute path)")
parser.add_option("--store_dir",  help="folder to store the output generated events")
parser.add_option("--pdfset",  default=90500, help="pdfset to be used. Possible values: 90500,13100,25200")
parser.add_option("--pdfsyst",  default=False, help="add nominal pdf internal variations", action="store_true")
parser.add_option("--seed",  default=1, help="generation seed")
parser.add_option("--driver",  default="direct", help=" direct or condor or qsub: submit single core or on condor/qsub batch queue")
parser.add_option("--njobs", default=10, help="number of jobs to split the generation. Jobs needs to run in less than one hour")
parser.add_option("--jobFlavour", default="longlunch", help="set the duration of the condor job, see https://batchdocs.web.cern.ch/local/submit.html")
parser.add_option("--tune",  default="mg5", help="shower tune: mg5, atlas_A14NNPDF23LO, cms_CP5")
parser.add_option("--clean",  default=False, help="remove all the auxiliary folders used for the calculation", action="store_true")
(options,args)=parser.parse_args()



if options.driver=="direct":

        runtrilinearstudy(proc=options.proc,nevents=options.nevents,mg5_dir=options.mg5_dir,trilinear_dir=options.trilinear_dir,
                        store_dir=options.store_dir,pdfset=options.pdfset,seed=options.seed,tune=options.tune,dummy_process_dir=options.dummy_process_dir,clean=options.clean, pdfsyst=options.pdfsyst)

if options.driver=="condor":

    # avoid conflicts between jobs trying to overwrite the same mg5 path
    os.system("cp -r %s/hhh-model %s/models/"%(options.trilinear_dir,options.mg5_dir))
    if options.proc == 'ttH':
        os.system("cp %s/tth-loop_diagram_generation.py %s/madgraph/loop/loop_diagram_generation.py"%(options.trilinear_dir,options.mg5_dir))
    else:
        os.system("cp %s/vvh-loop_diagram_generation.py %s/madgraph/loop/loop_diagram_generation.py"%(options.trilinear_dir,options.mg5_dir))

    os.system("mkdir %s"%(options.store_dir))
    os.chdir("%s"%(options.store_dir))
    os.system("mkdir condor_logs")
    store_dir=os.getcwd()
    if options.dummy_process_dir=="": dummy_process_dir=options.mg5_dir+"/test_ttbar"
    if not (os.path.isdir(dummy_process_dir) and os.path.isdir(dummy_process_dir+"/lib/Pdfdata") and os.path.isdir(dummy_process_dir+"/lib/PDFsets")) :
        dummy_process_dir=run_dummy_process (options.mg5_dir,str(options.pdfset).split(",")[0])
    clean=""
    if options.clean: clean="--clean"
    pdfsyst=""
    if options.pdfsyst: pdfsyst="--pdfsyst"
    subgenfile="submitcondor.vanilla"
    if os.path.isfile("submitcondor.vanilla") : os.system("rm submitcondor.vanilla")
    for i in range(0,int(options.njobs)): 
        store_dir_sub=store_dir+"/condor_%i"%i     
        template = ['Executable= %s'%workdir+"/utils/runtrilinearstudy.py",
        'output = {stdout}',
        'error = {stderr}',
        'log = {joblog}',
        'Universe = vanilla',
        'arguments = "--proc {proc} --nevents {nevents} --dummy_process_dir {dummy_process_dir} --trilinear_dir {trilinear_dir} --mg5_dir {mg5_dir} --pdfset {pdfset} --seed {seed} --store_dir {store_dir} --tune {tune}  %s %s"'%(clean,pdfsyst),
        'getenv = TRUE',
        # 'Requirements = RegExp("^proof-.*", Machine)',
        'Requirements = (OpSysAndVer =?= "CentOS7")',
        # 'Rank = Mips',
        'transfer_input_files    = {lheCombiner}',
        # "RequestMemory = 5000",
        '+JobFlavour = "{jobFlavour}"',
        ]
        if options.proc=="Hjj": memory="10000"
        else: memory="3000"
        template.append("queue\n")
        template = '\n'.join(template)
        template = template.format(proc=options.proc,nevents=str(int(options.nevents/int(options.njobs))),dummy_process_dir=dummy_process_dir,
        trilinear_dir=options.trilinear_dir,mg5_dir=options.mg5_dir,pdfset=str(options.pdfset),store_dir=store_dir_sub,seed=str(i+1),jobFlavour=options.jobFlavour,
        stdout=store_dir+"/condor_logs/log_out_%i.txt"%i,stderr=store_dir+"/condor_logs/log_err_%i.txt"%i,joblog=store_dir+"/condor_logs/log_%i.txt"%i, lheCombiner=workdir+"/utils/lheCombiner.py",tune=options.tune, memory=memory)
        with open(subgenfile, 'a') as f:
                f.write(template)
    
    command = 'condor_submit %s' % subgenfile
    os.system(command)
    os.chdir(workdir)
    print("\nYou can check how many yoda files exist (corresponding to successful jobs) \nwith the following command")
    print("for i in $(seq 0 %i); do ls %s/condor_$i/Rivet.yoda; done"%(int(options.njobs)-1,store_dir))
    print("\nTo merge the yoda files you can run the following command")
    print("yodamerge -o %s/Rivet_%s.yoda %s/condor_*/Rivet.yoda\n"%(store_dir,options.proc,store_dir))

if options.driver=="qsub":
    
    os.system("mkdir %s"%(options.store_dir))
    os.chdir("%s"%(options.store_dir))
    os.system("mkdir qsub_scripts")
    store_dir=os.getcwd()
    if options.dummy_process_dir=="": dummy_process_dir=options.mg5_dir+"/test_ttbar"
    if not (os.path.isdir(dummy_process_dir) and os.path.isdir(dummy_process_dir+"/lib/Pdfdata") and os.path.isdir(dummy_process_dir+"/lib/PDFsets")) :
        dummy_process_dir=run_dummy_process (options.mg5_dir,str(options.pdfset).split(",")[0])
    
    clean=""
    if options.clean: clean="--clean"
    pdfsyst=""
    if options.pdfsyst: pdfsyst="--pdfsyst"     
    for i in range(0,int(options.njobs)): 
        subgenfile="qsub_scripts/submit_qsub_%i.sh"%i
        store_dir_sub=store_dir+"/qsub_%i"%i     
        template=workdir+"/utils/runtrilinearstudy.py --proc {proc} --nevents {nevents} --dummy_process_dir {dummy_process_dir} --trilinear_dir {trilinear_dir} --mg5_dir {mg5_dir} --pdfset {pdfset} --seed {seed} --store_dir {store_dir} --tune {tune} %s %s"%(clean,pdfsyst)
        template = template.format(proc=options.proc,nevents=str(int(options.nevents/int(options.njobs))),dummy_process_dir=dummy_process_dir,
        trilinear_dir=options.trilinear_dir,mg5_dir=options.mg5_dir,pdfset=str(options.pdfset),store_dir=store_dir_sub,seed=str(i+1),tune=options.tune)
        with open(subgenfile, 'w') as f:
                f.write(template)
        os.system("chmod u+x %s"%subgenfile)
        command = 'qsub -V -q generic7  %s' % subgenfile
        print(command)
        os.system(command)
    os.chdir(workdir)
